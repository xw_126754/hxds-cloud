package com.example.hxds.dr.service;

import java.util.HashMap;

public interface DriverSettingsService {
    //查询司机设置
    public HashMap searchDriverSettings(long driverId);
}
